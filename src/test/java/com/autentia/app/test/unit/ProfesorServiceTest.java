package com.autentia.app.test.unit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.autentia.bean.ProfesorBean;
import com.autentia.dao.ProfesorRepository;

@RunWith(MockitoJUnitRunner.class)
public class ProfesorServiceTest {

	@Mock
	private ProfesorRepository profesorService;
	
	ProfesorBean profesor;
	
	@Before
	public void init() throws SQLException, Exception {

		profesor = new ProfesorBean();
		profesor.setId(1);
		profesor.setNombreProfesor("Pepe Salas");
	}
		
	
	@Test
	public void getProfesores(){
		List<ProfesorBean> lista = new ArrayList<ProfesorBean>();
		lista.add(profesor);
		
		Mockito.when(profesorService.selectAll()).thenReturn(lista);

		
		List<ProfesorBean> profesores =  profesorService.selectAll();
		Assert.assertNotNull(profesores);
		assertThat(profesores.get(0).getId(), is(1));
		assertThat(profesores.get(0).getNombreProfesor(), is("Pepe Salas"));
	}
}
