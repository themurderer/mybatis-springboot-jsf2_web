package com.autentia.app.test.integration;

import java.io.FileInputStream;
import java.util.List;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.autentia.app.common.BaseRepositoryTest;
import com.autentia.bean.ProfesorBean;
import com.autentia.dao.ProfesorRepository;

public class ProfesorServiceTest extends BaseRepositoryTest{
	@Autowired
	ProfesorRepository profesorService;
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/dataset/datasetProfesor.xml"));
	}
	
	@Test
	public void testSelectAllProfesores(){
		List<ProfesorBean> profesores = profesorService.selectAll();
		Assert.assertNotNull(profesores);
        Assert.assertEquals(2, profesores.size());
	}

}
