package com.autentia.app.test.integration;

import java.io.FileInputStream;
import java.util.List;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.autentia.app.common.BaseRepositoryTest;
import com.autentia.bean.CursoBean;
import com.autentia.dao.CursoRepository;

public class CursoServiceTest extends BaseRepositoryTest{

	@Autowired
	CursoRepository cursoService;
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/dataset/dataset.xml"));
	}
	
	@Test
	public void testSelectAllCursos(){
		List<CursoBean> cursos = cursoService.selectAll();
		Assert.assertNotNull(cursos);
        Assert.assertEquals(2, cursos.size());
	}
	


}
