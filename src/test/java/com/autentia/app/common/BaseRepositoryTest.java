package com.autentia.app.common;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.dbunit.DBTestCase;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.ext.hsqldb.HsqldbConnection;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/test-global.xml" })
public abstract class BaseRepositoryTest extends DBTestCase {
	
	@Autowired
	@Qualifier(value="dataSourceHSQL")
	private DataSource datasource;


	private Connection connection;
	private IDatabaseConnection dbunitConnection;

	/**
	 * Init test.
	 * 
	 * @throws Exception
	 * @throws SQLException
	 */
	@Before
	public void init() throws SQLException, Exception {
		connection = datasource.getConnection();
		dbunitConnection = new HsqldbConnection(connection, null);
		DatabaseOperation.CLEAN_INSERT.execute(dbunitConnection, getDataSet());
	}

	/**
	 * Clean test.
	 * 
	 * @throws SQLException
	 */
	@After
	public void clean() throws SQLException {
		// Close database connection after each method to avoid problem with a
		// lot of tests
		dbunitConnection.close();
		connection.close();
	}
}
