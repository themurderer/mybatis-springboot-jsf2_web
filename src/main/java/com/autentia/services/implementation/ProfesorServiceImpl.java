package com.autentia.services.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.autentia.bean.ProfesorBean;
import com.autentia.dao.ProfesorRepository;
import com.autentia.services.ProfesorService;

/**
 * Clase controladora del objeto Profesor
 * @author Ra�l
 *
 */

@Service("profesorServiceImpl")
public class ProfesorServiceImpl implements ProfesorService{

	@Autowired
	ProfesorRepository profesorRepository;

	
	public List<ProfesorBean> selectAll(){
		return profesorRepository.selectAll();
	}


	public ProfesorRepository getProfesorDAO() {
		return profesorRepository;
	}


	public void setProfesorDAO(ProfesorRepository profesorDAO) {
		this.profesorRepository = profesorDAO;
	}


	
	public ProfesorBean selectById(int id) {
		return profesorRepository.selectProfesorById(id);
	}
	
	
}
