package com.autentia.services.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.autentia.bean.CursoBean;
import com.autentia.dao.CursoRepository;
import com.autentia.services.CursoService;


/**
 * Clase controladora que gestiona el catalogo de cursos
 * @author Ra�l S.
 *
 */

@Service("cursoServiceImpl")
public class CursoServiceImpl implements CursoService{
	
	
	@Autowired
	private CursoRepository cursoRepository;

	
	@Override
	public List<CursoBean> selectAll()
	{
		return cursoRepository.selectAll();
	}
	
	
	@Override
	public void create(CursoBean c){
		cursoRepository.insert(c);
	}


	public CursoRepository getCursoRepository() {
		return cursoRepository;
	}


	public void setCursoRepository(CursoRepository cursoRepository) {
		this.cursoRepository = cursoRepository;
	}



	 
	 
}

