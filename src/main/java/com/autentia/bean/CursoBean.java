package com.autentia.bean;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.Base64;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Bean;

import com.autentia.common.BusinessEntity;

/**
 * Implementaci�n del bean Curso
 * @author Ra�l S.
 *
 */


public class CursoBean extends BusinessEntity implements Serializable {

	
	private static final long serialVersionUID = -1798381287217382209L;


	private String nombreCurso;
	private int nivelCurso;
	private int horasCurso;
	private Boolean estadoCurso;
	private ProfesorBean profesor;
	private static String[] categoria = { "B�sico", "Intermedio", "Avanzado" };
	private String fileBase64;
	private String nombreFichero;
	
	private  StreamedContent file;

	public CursoBean() {
		nivelCurso = 1;
		horasCurso = 1;
		estadoCurso = true;
		setFileBase64(new String());
	}

	public CursoBean(int id, String nombreCurso, Integer nivelCurso,
			Integer horasCurso, Boolean estadoCurso, ProfesorBean profesor) {
		super();
		setId(id);
		this.nombreCurso = nombreCurso;
		this.nivelCurso = nivelCurso;
		this.horasCurso = horasCurso;
		this.estadoCurso = estadoCurso;
		this.profesor = profesor;
	}

	public String getNombreCurso() {
		return nombreCurso;
	}

	public void setNombreCurso(String nombreCurso) {
		this.nombreCurso = nombreCurso;
	}

	public Integer getNivelCurso() {
		return nivelCurso;
	}

	public void setNivelCurso(Integer nivelCurso) {
		this.nivelCurso = nivelCurso;
	}

	public String getNivelCursoCad() {
		return categoria[(nivelCurso - 1)];
	}

	public Integer getHorasCurso() {
		return horasCurso;
	}

	public void setHorasCurso(Integer horasCurso) {
		this.horasCurso = horasCurso;
	}

	public Boolean getEstadoCurso() {
		return estadoCurso;
	}

	public void setEstadoCurso(Boolean estadoCurso) {
		this.estadoCurso = estadoCurso;
	}

	public ProfesorBean getProfesor() {
		return profesor;
	}

	public void setProfesor(ProfesorBean p) {
		this.profesor = p;
	}
	
	public String getFileBase64() {
		return fileBase64;
	}

	public void setFileBase64(String fileBase64) {
		this.fileBase64 = fileBase64;
	}

	public String getNombreFichero() {
		return nombreFichero;
	}

	public void setNombreFichero(String nombreFichero) {
		this.nombreFichero = nombreFichero;
	}
	
	public void setFile(StreamedContent file){
		this.file = file;
	}

	@Override
	public int hashCode(){
	    return new HashCodeBuilder()
	        .append(getId())
	        .toHashCode();
	}

	@Override
	public boolean equals(final Object obj){
	    if(obj instanceof Bean){
	        final ProfesorBean other = (ProfesorBean) obj;
	        return new EqualsBuilder()
	            .append(getId(), other.getId())
	            .isEquals();
	    } else{
	        return false;
	    }
	}
	
	
	/*
	 * Metodo para obtener el fichero (Codificado en Base64) cuando se pretende descargar.
	 * @return Contenido del fichero 
	 */

	public StreamedContent getFile() {
		
		if (fileBase64 != null && file == null) {
			file = new DefaultStreamedContent(new ByteArrayInputStream(Base64
					.getDecoder().decode(fileBase64)),
					"application/octet-stream", nombreFichero);
		}
		return file;
	}

	
	
	
	public boolean isDownloader(){
		return (nombreFichero != null);
	}
	




}
