package com.autentia.interfaces.web.bean;

import java.io.Serializable;
import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 * 
 * 
 * @author Raul S
 */
@ManagedBean
@SessionScoped
public class LocaleBean implements Serializable {
	
	
	private static final long serialVersionUID = -6340340499384728584L;

	
	private Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();

	
	public void setLanguage(String language) {
		locale = new Locale(language);
		FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
	}

	public Locale getLocale() {
		return locale;
	}

	
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

}