package com.autentia.dao;

import java.util.List;

import com.autentia.bean.ProfesorBean;


/**
 * Clase de gestion select, update y delete de PROFESOR en base de datos
 * @author Ra�l
 *
 */
public interface ProfesorDAO{


    public List<ProfesorBean> selectAll();
       

    public ProfesorBean selectProfesorById(int idPr);

  
   
}

