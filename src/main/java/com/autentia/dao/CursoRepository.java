package com.autentia.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.autentia.bean.CursoBean;

@Repository
public interface CursoRepository extends CursoDAO {
	
	
	@Select(" SELECT idCurso, nombreCurso, nivelCurso, horasCurso, estadoCurso, fileBase64, nombreFichero "
			+ " FROM curso"
			+ " INNER JOIN profesor ON curso.Profesor_idProfesor = profesor.idProfesor"
			+ " WHERE estadoCurso=1")
	@Results(value={
			@Result(id = true, property = "id", column = "idCurso"),
			@Result(property = "nombreCurso", column = "nombreCurso"),
			@Result(property = "nivelCurso", column = "nivelCurso"),
			@Result(property = "horasCurso", column = "horasCurso"),
			@Result(property = "estadoCurso", column = "estadoCurso"),
			@Result(property = "profesor", column = "Profesor_idProfesor",one=@One(select="com.autentia.dao.ProfesorRepository.selectProfesorById")),
			@Result(property = "fileBase64", column = "fileBase64"),
			@Result(property = "nombreFichero", column = "nombreFichero") })
	@Override
	public List<CursoBean> selectAll();

	@Select(" SELECT idCurso, nombreCurso, nivelCurso, horasCurso, estadoCurso, fileBase64, nombreFichero" 
	        +" FROM curso"
	        +" INNER JOIN profesor ON curso.Profesor_idProfesor = profesor.idProfesor WHERE IdCurso = #{id}")
	public CursoBean selectCursoById(int id);

	@Insert("  INSERT INTO curso (nombreCurso, nivelCurso, horasCurso,estadoCurso,Profesor_idProfesor, fileBase64, nombreFichero)"
			+ " VALUES (#{nombreCurso},#{nivelCurso},#{horasCurso},#{estadoCurso},#{profesor.id},#{fileBase64},#{nombreFichero});")
	@Options(useGeneratedKeys = true, keyProperty = "id", flushCache = true, keyColumn = "id")
	@Override
	public void insert(CursoBean curso);
}
