	package com.autentia.dao;

import java.util.List;

import com.autentia.bean.CursoBean;

/**
 * Clase de gestion de select, update y delete de UPDATE con Base de datos
 * @author Ra�l S.
 *
 */
public interface CursoDAO{


    public List<CursoBean> selectAll();
        
	
    public CursoBean selectCursoById(int id);


    public void insert(CursoBean curso);




 
 

}